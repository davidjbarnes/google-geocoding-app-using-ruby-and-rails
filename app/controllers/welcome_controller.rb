require 'net/http'
require 'json'

class WelcomeController < ApplicationController
  def index
    @locations = Location.all
  end

  def create
    url = URI("https://maps.googleapis.com/maps/api/geocode/json?address=#{params[:address]}")
    res = Net::HTTP::get_response(url)
    response = res.body if res.is_a?(Net::HTTPSuccess)
    data = JSON.parse(response)
    location = data["results"][0]["geometry"]["location"]
    
    l = Location.create(lat: location["lat"], lng: location["lng"])
    l.save!
  end
end
