class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.text :lat
      t.text :lng

      t.timestamps
    end
  end
end
